﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using TMPro;

public class Health : NetworkBehaviour
{
    public const int maxHealth = 100;
    public RectTransform healthbar;
    [SyncVar(hook = "OnChangeHealth")]public int currentHealth = maxHealth;
    [SyncVar]
    private Transform transformspanwposition;
    public bool destroyOnDeath;
    private NetworkStartPosition[] spawnPoints;
    public GameObject FloatingTextPrefab;

    private void Start()
    {
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        }
    }

    public void TakeDamage(int amount)
    {
        if (!isServer)
        {
            return;
        }
        if (FloatingTextPrefab != null)
        {
            RpcShowFloatingText(amount);
        }
        currentHealth -= amount;
        if (currentHealth <= 0) {
            if (destroyOnDeath)
            {
                if (gameObject.GetComponent<Animator>())
                {
                    gameObject.GetComponent<Animator>().SetBool("Death", true);
                }
                if (gameObject.GetComponent<NavMeshAgent>())
                {
                    gameObject.GetComponent<NavMeshAgent>().isStopped = true;
                }
                if (gameObject.GetComponent<CapsuleCollider>())
                {
                    gameObject.GetComponent<CapsuleCollider>().enabled = false;
                }
                if (gameObject.GetComponent<Rigidbody>())
                {
                    gameObject.GetComponent<Rigidbody>().useGravity = false;
                }
                Destroy(gameObject,5);
            }
            else
            {
                currentHealth = maxHealth;
                if (gameObject.GetComponent<NavMeshAgent>())
                {
                    gameObject.GetComponent<NavMeshAgent>().isStopped = false;
                }
                if (gameObject.GetComponent<CapsuleCollider>())
                {
                    gameObject.GetComponent<CapsuleCollider>().enabled = true;
                }
                if (gameObject.GetComponent<Rigidbody>())
                {
                    gameObject.GetComponent<Rigidbody>().useGravity = true;
                }
                RpcRespawn();
                gameObject.GetComponent<Animator>().SetBool("Death", false);
            }
        }
    }
    void OnChangeHealth(int health)
    {
        healthbar.sizeDelta = new Vector2(health * 2, healthbar.sizeDelta.y);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            Vector3 spawnPoint = Vector3.zero;
            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }
            transform.position = spawnPoint;
        }
    }

    [ClientRpc]
    void RpcShowFloatingText(int amount)
    {
        if (!isLocalPlayer)
        {
            transformspanwposition = transform;
            GameObject floatingText = Instantiate(FloatingTextPrefab, transformspanwposition.position, Quaternion.identity, transform);
            floatingText.GetComponent<TextMeshPro>().text = amount.ToString();
            //NetworkServer.Spawn(floatingText);
        }
    }
}
