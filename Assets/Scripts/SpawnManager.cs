﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class SpawnManager : NetworkBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    private GameObject[] enemySpawns;
    private int counter;
    public int numberOfEnemies = 15;
    private int maxNumberOfZombies = 20;
    private float waveRate = 5;
    private bool isSpawnActivated = true;

    public override void OnStartServer()
    {
        enemySpawns = GameObject.FindGameObjectsWithTag("ZombieSpawn");
        StartCoroutine(ZombieSpawner());
    }

    IEnumerator ZombieSpawner()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(waveRate);
            GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
            if (zombies.Length < maxNumberOfZombies)
            {
                CommenceSpawn();
            }
        }
    }
    void SpawnZombies(Vector3 spawnPos)
    {
        counter++;
        //enemySpawn.transform.position += new Vector3(Random.Range(-8.0f, 8.0f), 0.0f, Random.Range(-8.0f, 8.0f));

        GameObject enemy = (GameObject)Instantiate(enemyPrefab, spawnPos, Quaternion.identity);
        enemy.GetComponent<ZombieID>().enemyID = "Zombie" + counter;
        NetworkServer.Spawn(enemy);
    }

    void CommenceSpawn()
    {
        if (isSpawnActivated)
        {
            for (int i = 0; i < numberOfEnemies; i++)
            {
                int randomindex = Random.Range(0, enemySpawns.Length);
                SpawnZombies(enemySpawns[randomindex].transform.position);
            }
        }
    }
}
