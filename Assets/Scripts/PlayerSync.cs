﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSync : NetworkBehaviour
{

    [SyncVar] private Vector3 syncPos;

    [SerializeField] Transform myTransform;
    [SerializeField] float lerprate = 15;

    private Vector3 lastPos;
    private float threshold = 0.5f; //half unity unit (m)
    private void Update()
    {
        LerpPosition();
    }
    private void FixedUpdate()
    {
        TransmitPosition();
    }
    void LerpPosition()
    {
        //Lerp position of only client players (only visual!!)
        if (!isLocalPlayer)
        {
            myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerprate);
        }
    }
    //Tell the server where we currently are
    [Command]
    void CmdProvidePositionToServer(Vector3 pos)
    {
        syncPos = pos;
    }

    [ClientCallback]
    void TransmitPosition()
    {
        //if is our player (client) and has moved the threshold distance
        if (isLocalPlayer && Vector3.Distance(transform.position, lastPos) > threshold)
        {
            CmdProvidePositionToServer(myTransform.position);
            lastPos = myTransform.position;
        }
    }
}
