﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerID : NetworkBehaviour
{

    [SyncVar]public string playerUniqueName;
    private NetworkInstanceId playerNetID;
    private Transform mytransform;
    public override void OnStartLocalPlayer()
    {
        GetNetIdentity();
        SetName();
    }

    private void Awake()
    {
        mytransform = transform;
    }
    private void Update()
    {
        if (mytransform.name == "" || mytransform.name == "Player(Clone)")
        {
            SetName();
        }
    }
    [Client]
    void GetNetIdentity()
    {
        playerNetID = GetComponent<NetworkIdentity>().netId;
        CmdtellServerMyName(MakeUniqueName());
    }

    string MakeUniqueName()
    {
        string uniqueName = "Player" + playerNetID.ToString();
        return uniqueName;
    }

    [Command]
    void CmdtellServerMyName(string name)
    {
        playerUniqueName = name;
    }

    void SetName()
    {
        if (!isLocalPlayer)
        {
            mytransform.name = playerUniqueName;
        }
        else
        {
            mytransform.name = MakeUniqueName();
        }
    }
}
