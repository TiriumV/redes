﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Zombie_Attack : NetworkBehaviour
{
    private float attackRate = 2;
    private float nextAttack;
    private int damage = 10;
    private float minDistance = 1;
    private float currentDistance;
    private Zombie_Target targetScript;

    // Start is called before the first frame update
    void Start()
    {
        targetScript = GetComponent<Zombie_Target>();
        if (isServer)
        {
            StartCoroutine(Attack());
        }
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Animator>().SetFloat("Speed", transform.GetComponent<Rigidbody>().velocity.magnitude);
    }

    IEnumerator Attack()
    {
        for (; ; )
        {
            yield return new WaitForSeconds(0.2f);
            CheckIfTargetInRange();
        }
    }
    void CheckIfTargetInRange()
    {
        if (targetScript.targetTransform != null)
        {
            currentDistance = Vector3.Distance(targetScript.targetTransform.position, transform.position);
            if (currentDistance < minDistance && Time.time > nextAttack && gameObject.GetComponent<Health>().currentHealth >0)
            {
                nextAttack = Time.time + attackRate;
                targetScript.targetTransform.GetComponent<Health>().TakeDamage(damage);
                StartCoroutine(ChangeZombieAnimator()); //Onlyforhost
                RpcChangeZombieAnimator();
            }
        }
    }
    [ClientRpc]
    void RpcChangeZombieAnimator()
    {
        StartCoroutine(ChangeZombieAnimator());
    }

    IEnumerator ChangeZombieAnimator()
    {
        gameObject.GetComponent<Animator>().SetBool("Attack", true);
        yield return new WaitForSeconds(attackRate / 2);
        gameObject.GetComponent<Animator>().SetBool("Attack", false);
    }
}
