﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Zombie_Motion_Sync : NetworkBehaviour
{
    [SyncVar] private Vector3 syncPos;
    [SyncVar] private float syncYRot;

    private Vector3 lastPos;
    private Quaternion lastrotation;
    private float lerpRate = 10;
    private float positionThreshhold = 0.5f;
    private float rotThreshHold = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TransmitMotion();
        LerpMotion();
    }
    void TransmitMotion()
    {
        if (!isServer)
        {
            return;
        }
        //If position and rotation are greater than treshhold, update position and rotation
        if (Vector3.Distance(transform.position, lastPos) > positionThreshhold || Quaternion.Angle(transform.rotation, lastrotation) > rotThreshHold)
        {
            lastPos = transform.position;
            lastrotation = transform.rotation;

            syncPos = transform.position;
            syncYRot = transform.localEulerAngles.y;
        }
    }
    void LerpMotion() {
        //Interpolate only on clients
        if (isServer)
        {
            return;
        }
        transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * lerpRate);

        Vector3 newRot = new Vector3(0, syncYRot, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(newRot), Time.deltaTime * lerpRate);
    }
}
