﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ZombieID : NetworkBehaviour 
{
    [SyncVar] public string enemyID;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetIdentity();
    }
    void SetIdentity()
    {
        if (transform.name == "" || transform.name == "TT_demo_zombie(Clone)")
        {
            transform.name = enemyID;
        }
    }
}
