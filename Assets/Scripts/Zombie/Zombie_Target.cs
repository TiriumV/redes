﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class Zombie_Target : NetworkBehaviour
{

    private NavMeshAgent agent;
    public Transform targetTransform;
    private LayerMask raycastLayer;
    private float radius = 100;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent < NavMeshAgent >();
        raycastLayer = 1<<LayerMask.NameToLayer("Player");
        if (isServer) //Only server is responsable for zombie AI
        {
            StartCoroutine(DoCheck());
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }
    void SearchForTarget()
    {
        //Code only for server
        if (!isServer)
        {
            return;
        }
        if (targetTransform == null)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, raycastLayer);
            //Pick random target
            if (hitColliders.Length > 0)
            {
                int randomint = Random.Range(0, hitColliders.Length);
                targetTransform = hitColliders[randomint].transform;
            }
        }
        if (targetTransform != null && targetTransform.GetComponent<CapsuleCollider>().enabled == false)
        {
            targetTransform = null;
        }
    }
    void MoveToTargets()
    {
        if (targetTransform != null && isServer)
        {
            SetNavDestination(targetTransform);
        }
    }

    void SetNavDestination(Transform dest) {
        agent.SetDestination(dest.position);
    }

    IEnumerator DoCheck()
    {
        for (; ; )
        {
            SearchForTarget();
            MoveToTargets();
            yield return new WaitForSeconds(0.2f);
        }
    }
}
