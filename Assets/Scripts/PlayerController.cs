﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour
{
    public AudioSource shootsource;
    public AudioSource FlamesAudioSource;
    private Rigidbody rb;
    public float speed;
    public GameObject bulletPrefab;
    public ParticleSystem muzzle;
    public ParticleSystem flamethorwer;
    public Transform bulletSpawntrf;
    public GameObject playercameraPrefab;
    public GameObject FalemTrowerFlames;
    Transform playerCameraTransform;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    public GameObject myCamera;
    public Animator myanimator;
    void Start()
    {
        myanimator = GetComponent<Animator>();
        if (!isLocalPlayer)
        {
            return;
        }
        playerCameraTransform = transform.GetChild(0).transform;
        
        rb = GetComponent<Rigidbody>();
        myCamera = Instantiate(playercameraPrefab, playerCameraTransform.position, playerCameraTransform.rotation);
        myCamera.GetComponent<SmoothCameraMovement>().PlayerTransform = gameObject.transform;
    }
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        myCamera.GetComponent<SmoothCameraMovement>().PlayerTransform = transform;
        moveInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        moveVelocity = moveInput * speed;
        Ray cameraRay = myCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);
            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
        if (bulletSpawntrf != null)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                CmdFire();
            }
        }
        if (transform.GetChild(3).transform.name == "Body_german_C")
        {
            if (Input.GetMouseButtonDown(0)){
                CmdFireFlames();
            }
            if (Input.GetMouseButtonUp(0))
            {
                CmdStopFlames();
            }

        }
    }
    void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        rb.velocity = new Vector3(moveVelocity.x, rb.velocity.y, moveVelocity.z);
        myanimator.SetFloat("Velocity", Mathf.Abs(rb.velocity.magnitude));
    }

    public override void OnStartLocalPlayer()
    {
        transform.Find("Healthbar Canvas").GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.yellow;
        base.OnStartLocalPlayer();
    }

    [Command]
    public void CmdFire() {
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawntrf.position, bulletSpawntrf.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 26.0f;
        Destroy(bullet, 2);
        NetworkServer.Spawn(bullet);

        RpcEffects();
    }
    [Command]
    public void CmdFireFlames()
    {
        RpcEffectsFlame();
    }

    [ClientRpc]
    void RpcEffects()
    {
        muzzle.Play();
        shootsource.Play();
    }

    [ClientRpc]
    void RpcEffectsFlame()
    {
        FalemTrowerFlames.GetComponent<CapsuleCollider>().enabled = true;
        flamethorwer.Play();
        FlamesAudioSource.Play();
    }

    [Command]
    void CmdStopFlames()
    {
        RpcStopFlames();
    }

    [ClientRpc]
    void RpcStopFlames()
    {
        FalemTrowerFlames.GetComponent<CapsuleCollider>().enabled = false;
        flamethorwer.Stop();
        FlamesAudioSource.Stop();
    }
}
