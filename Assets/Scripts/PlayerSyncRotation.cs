﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSyncRotation : NetworkBehaviour
{
    [SyncVar] private Quaternion syncPlayerRotation;

    [SerializeField] private Transform playerTransform;
    [SerializeField] private float lerpRate = 15;

    private Quaternion lastPlayerRot;
    private float threshold = 7f;
    private void FixedUpdate()
    {
        TransmitRotations();
    }
    private void Update()
    {
        LerpRotations();
    }

    void LerpRotations()
    {
        if (!isLocalPlayer)
        {
            playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
        } 
    }

    [Command]
    void CmdProvideRotationsToServer(Quaternion playerRot)
    {
        syncPlayerRotation = playerRot;
    }
    //Only Runs on client
    [Client]
    void TransmitRotations()
    {
        if (isLocalPlayer)
        {
            if (Quaternion.Angle(playerTransform.rotation, lastPlayerRot) > threshold)
            {
                CmdProvideRotationsToServer(playerTransform.rotation);
                lastPlayerRot = playerTransform.rotation;
            }
        }
    }
}
