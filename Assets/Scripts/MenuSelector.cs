﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSelector : MonoBehaviour
{
    public GameObject NetworkManager;
    // Start is called before the first frame update
    void Start()
    {
        NetworkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        NetworkManager.GetComponent<NetWorkManagement>().SelectCharacter(0);
    }

    // Update is called once per frame
    public void SetValue(int index)
    {
        NetworkManager.GetComponent<NetWorkManagement>().SelectCharacter(index);
    }
}
