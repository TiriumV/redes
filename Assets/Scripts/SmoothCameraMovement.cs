﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SmoothCameraMovement : NetworkBehaviour
{
    public Transform PlayerTransform;
    private Vector3 _cameraOffset;
    private bool once;
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        once = true;
    }

    void FixedUpdate()
    {
        if(PlayerTransform == null){
            Destroy(gameObject);
        }
        else
        {
            if (once)
            {
                _cameraOffset = transform.position - PlayerTransform.position;
                once = false;
            }
            Vector3 newPos = PlayerTransform.position + _cameraOffset;
            transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
        }
    }
}
