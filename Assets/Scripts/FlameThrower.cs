﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrower : MonoBehaviour
{

    // Start is called before the first frame update
    private void OnTriggerStay(Collider other)
    {
        GameObject hit = other.gameObject;
        Health health = hit.GetComponent<Health>();
        if (health != null)
        {
            health.TakeDamage(Random.Range(1, 3));
        }
        //Destroy(gameObject);
    }
}
