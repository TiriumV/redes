﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerNetworkAnimations : NetworkBehaviour
{
    public NetworkAnimator networkAnim;
    // Start is called before the first frame update
    public override void OnStartLocalPlayer()
    {
        /*
        Renderer[] rens = GetComponentInChildren<Renderer>();
        foreach (Renderer rend in rens)
        {
            rend.enabled = false;
        }
        */
        networkAnim.SetParameterAutoSend(0, true);

    }

    public override void PreStartClient()
    {
        networkAnim.SetParameterAutoSend(0, true);
    }
}
