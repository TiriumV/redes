﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking.NetworkSystem;


public class NetWorkManagement : NetworkManager
{
    // in the Network Manager component, you must put your player prefabs 
    // in the Spawn Info -> Registered Spawnable Prefabs section 
    public int index = 0;
    public GameObject[] playerPrefabList;
    public class MsgTypes
    {
        public const short PlayerPrefab = MsgType.Highest + 1;

        public class PlayerPrefabMsg : MessageBase
        {
            public short controllerID;
            public short prefabIndex;
        }
    }

    public override void OnStartServer()
    {
        NetworkServer.RegisterHandler(MsgTypes.PlayerPrefab, OnResponsePrefab);
        base.OnStartServer();
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        // A custom identifier we want to transmit from client to server on connection
        //int id = GetCustomValue();

        // Create message which stores our custom identifier
        IntegerMessage msg = new IntegerMessage(index);

        if (!clientLoadedScene)
        {
            // Call Add player and pass the message
            ClientScene.AddPlayer(conn, 0, msg);
        }
    }


    private void OnRequestPrefab(NetworkMessage netMsg)
    {
        MsgTypes.PlayerPrefabMsg msg = new MsgTypes.PlayerPrefabMsg();
        msg.controllerID = netMsg.ReadMessage<MsgTypes.PlayerPrefabMsg>().controllerID;
        msg.prefabIndex = (short)index;
        client.Send(MsgTypes.PlayerPrefab, msg);
    }
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        // Create message which stores our custom identifier
        IntegerMessage msg = new IntegerMessage(index);

        // always become ready.
        ClientScene.Ready(conn);

        bool addPlayer = (ClientScene.localPlayers.Count == 0);
        bool foundPlayer = false;
        for (int i = 0; i < ClientScene.localPlayers.Count; i++)
        {
            if (ClientScene.localPlayers[i].gameObject != null)
            {
                foundPlayer = true;
                break;
            }
        }
        if (!foundPlayer)
        {
            // there are players, but their game objects have all been deleted
            addPlayer = true;
        }
        if (addPlayer)
        {
            // Call Add player and pass the message
            ClientScene.AddPlayer(conn, 0, msg);
        }
    }
    private void OnResponsePrefab(NetworkMessage netMsg)
    {
        MsgTypes.PlayerPrefabMsg msg = netMsg.ReadMessage<MsgTypes.PlayerPrefabMsg>();
        playerPrefab = spawnPrefabs[msg.prefabIndex];
        base.OnServerAddPlayer(netMsg.conn, msg.controllerID);
        //Debug.Log(playerPrefab.name + " spawned!");
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        // Variable to store the identifier
        int id = 0;

        // Read client message and receive identifier
        if (extraMessageReader != null)
        {
            var i = extraMessageReader.ReadMessage<IntegerMessage>();
            id = i.value;
        }

        // Select a specific prefab using the identifier (e.g. available prefabs could be stored in an array)
        GameObject playerPrefab = playerPrefabList[id];

        // Create player object with prefab
        GameObject player = (GameObject)Instantiate(playerPrefab, NetworkManager.singleton.GetStartPosition().position, Quaternion.identity);

        // Add player object for connection
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }

    // I have put a toggle UI on gameObjects called PC1 and PC2 to select two different character types.
    // on toggle, this function is called, which updates the playerPrefabIndex
    // The index will be the number from the registered spawnable prefabs that 
    // you want for your player
    public void SelectCharacter(int val)
    {
        index = (short)val;
    }
}